import sys
import calcoohija
import csv

with open(sys.argv[1], newline="") as archivo:
    lista = csv.reader(archivo)

    for line in lista:

        operacion = line[0]
        valortemporal = int(line[1])

        for numero in line[2:]:

            operador = int(numero)
            objeto = calcoohija.CalculadoraHija(valortemporal, operador)

            if operacion == "suma":
                valortemporal = objeto.plus()
            elif operacion == "resta":
                valortemporal = objeto.minus()
            elif operacion == "mult":
                valortemporal = objeto.mult()
            elif operacion == "div":
                valortemporal = objeto.div()
            else:
                sys.exit("Seleccione suma, resta, div o mult")
        print(valortemporal)
