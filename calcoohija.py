import sys
from calcoo import Calculadora


class CalculadoraHija(Calculadora):

    def mult(self):
        return self.numero1 * self.numero2

    def div(self):
        try:
            return self.numero1 / self.numero2
        except ZeroDivisionError:
            sys.exit("Division by zero is not allowed")


if __name__ == "__main__":

    try:
        operando1 = int(sys.argv[1])
        operando2 = int(sys.argv[3])

    except ValueError:
        sys.exit("Error: Non numerical parameters")

        objeto = CalculadoraHija(operando1, operando2)

        if sys.argv[2] == "suma":
            result = objeto.plus()
        elif sys.argv[2] == "resta":
            result = objeto.minus()
        elif sys.argv[2] == "mult":
            result = objeto.multi()
        elif sys.argv[2] == "div":
            result = objeto.div()
        else:
            sys.exit('Seleccione Suma,Resta, Multiplicar o Dividir')

        print(result)
