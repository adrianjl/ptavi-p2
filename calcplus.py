import sys
import calcoohija
import csv


if __name__ == "__main__":
    archivo = open(sys.argv[1])
    lista = archivo.readlines()

    for line in lista:
        elemento = line.split(",")
        operacion = elemento[0]
        valortemporal = int(elemento[1])

        for numero in elemento[2:]:

            operador = int(numero)
            objeto = calcoohija.CalculadoraHija(valortemporal, operador)

            if operacion == "suma":
                valortemporal = objeto.plus()
            elif operacion == "resta":
                valortemporal = objeto.minus()
            elif operacion == "mult":
                valortemporal = objeto.mult()
            elif operacion == "div":
                valortemporal = objeto.div()
            else:
                sys.exit("Seleccione suma, resta, div o mult")
        print(valortemporal)
